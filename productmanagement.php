<?php
include('includes/security.php'); 
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Product Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="productcode.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> Name </label>
                <input type="text" name="name" class="form-control" placeholder="Enter Product Name">
            </div>
            <div class="form-group">
                <label>Price</label>
                <input type="text" name="price" class="form-control" placeholder="Enter Price">
            </div>
            <div class="form-group">
                <label>Description</label>
                <input type="text" name="des" class="form-control" placeholder="Enter Description">
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" name="image" class="form-control" placeholder="Enter Image">
            </div>
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

<!-- DataTales Example -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add Product
            </button>
    </h6>
  </div>

  <div class="card-body">

  <?php 

     if (isset($_SESSION['success'])&&$_SESSION['status']!='')
       {
        echo '<h2.'.$_SESSION['success'] .'</h2>';
        unset($_SESSION['success']);
       }

   ?>

    <div class="table-responsive">


      <?php 
        

                     $limit = 10;  
                   if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
                      $start_from = ($page-1) * $limit;  

                      $query = "SELECT * FROM product ORDER BY id ASC LIMIT $start_from, $limit";  
                //  $rs_result = mysqli_query($conn, $sql);
                //  $query="SELECT * FROM user";
                       $query_run=mysqli_query($connection, $query);



    ?>

      <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">

          
          

        <thead>
          <tr class="header"> 
            <th> ID </th>
            <th> Name </th>
            <th>Price </th>
            <th>Description</th>
            <th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>
     
           <?php 

            if (mysqli_num_rows($query_run)>0 ) {
              # code...

              while ($row = mysqli_fetch_assoc($query_run)) {
                # code...
                ?>
              
               <tr>
     

                    <td><?php echo $row["id"]; ?></td>
                      
                    <td><?php echo $row["name"] ?></td>

                    <td><?php echo $row["price"]; ?></td>

                    <td><?php echo $row["description"]; ?></td>

                        <td>
                <form action="" method="post">
                    <input type="hidden" name="edit_id" value="">
                    <button  type="submit" name="edit_btn" class="btn btn-success"> EDIT</button>
                </form>
            </td>
            <td>
                <form action="" method="post">
                  <input type="hidden" name="delete_id" value="">
                  <button type="submit" name="delete_btn" class="btn btn-danger"> DELETE</button>
                </form>
            </td>
              </tr>

                  <?php 
              }
            }
          
                    ?>

        
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->



<?php  
$sql = "SELECT COUNT(id) FROM user";  
$rs_result = mysqli_query($connection, $sql);  

$row = mysqli_fetch_row($rs_result);  
$total_records = $row[0];  
$total_pages = ceil($total_records / $limit);  
$pagLink = "<nav><ul class='pagination'>";  
for ($i=1; $i<=$total_pages; $i++) {  
             $pagLink .= "<li><a href='register.php?page=".$i."'>".$i."</a></li>";  
};  

?>

<table class="table table-bordered" id="myTable1" width="100%" cellspacing="0">

<tr>
<td>

<li>
<?php
//echo $pagLink . "</ul></nav>";  



//echo '<div style="font-size:5.25em;color:red">'.$pagLink.'</div>';

echo '<div style="font-size:1.25em;color:#0e3c68;font-weight:bold;"><span style="font-size:2.25em;color:#0e3c68;font-weight:bold;">'.$pagLink.'</span></div>';
?>
</li>
</td>
</tr>

</table>


<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>



<script type="text/javascript">
$(document).ready(function(){
$('.pagination').pagination({
        items: <?php echo $total_records;?>,
        itemsOnPage: <?php echo $limit;?>,
        cssStyle: 'light-theme',
		currentPage : <?php echo $page;?>,
		hrefTextPrefix : 'register.php?page='
    });
	});
</script>


<?php
include('includes/scripts.php');
include('includes/footer.php');
?>